#define _GNU_SOURCE
#include <stdio.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
typedef struct {
	pid_t pid;
	int fd[2];
	char pname[32];
	int ready_time;
	int exec_time;
	struct timespec time[2];
	int flag;
}jobs;

#ifndef FIFO_H
#define FIFO_H


int FIFO();

#endif
