#include <string.h>
#include <stdio.h>
#include "sjf.h"
#include "fifo.h"
#include "rr.h"

int main(){

    char Poly[4][5]={"FIFO","RR","SJF","PSJF"};
    char Poly_in[5];


    scanf("%s",Poly_in);
    int i;
    for(i=0;i<4 ;i++)
        if(strcmp(Poly[i],Poly_in)==0)
            break;


    if(i==0)FIFO();
    if(i==1)RR();
    if(i==2)PORSJF(0);
    if(i==3)PORSJF(1);


    return 0;

}
