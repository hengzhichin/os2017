#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/syscall.h>
#include <linux/kernel.h>
#include <sched.h>
#include "rr.h"

void wait_for_interval() {
	volatile unsigned long i;
	for (i = 0; i<1000000UL; i++);
}

void wake_up (int pid) {
	struct sched_param param;
	param.sched_priority= 99;
	if (sched_setscheduler(pid, SCHED_FIFO, &param) < 0) {
		printf("Cannot wake child [%d]!\n", pid);
		exit(1);
	}
}

void pause_exec (int pid) {
	struct sched_param param;
	param.sched_priority = 20;
	if (sched_setscheduler(pid, SCHED_FIFO, &param) < 0) {
		printf("Cannot pause child [%d]!\n", pid);
		exit(2);
	}
}

// sort jobs by ready time while creating job list
void insertJob(job *head, job *newJob) {
	job *current = head;

	if (current->next == NULL)
		current->next = newJob;

	else {
		job *previous_job = NULL;

        	while (current != NULL && current->ready_time <= newJob->ready_time) {
        		previous_job = current;
        		current = current->next;
        	}

        	newJob->next = current;
        	previous_job->next = newJob;
    	}
} 

static int fork_child(job *current) {
	int child_id = fork();
	if (child_id == 0) {			
		// child
		clock_gettime(0, &(current->start_time));
		printf("%s %d\n", current->pTag, getpid());
		int exec_time = current->exec_time;
		    
		while (exec_time > 0) {
			wait_for_interval();
			exec_time--;

			// [after every clocktime] let parent to have control
			pause_exec(getpid());
		}
		    
		clock_gettime(0, &(current->end_time));
		char str[100];
		sprintf(str, "[Project 1] %d %ld.%ld %ld.%ld\n", getpid(), current->start_time.tv_sec, current->start_time.tv_nsec, current->end_time.tv_sec, current->end_time.tv_nsec);
		syscall(340, str);
		exit(0);
	}
	
	else {
		// parent
		return child_id;
	}
}

 int RR() {
	cpu_set_t cpuMask;
	CPU_ZERO(&cpuMask);
	CPU_SET(0, &cpuMask);
	if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuMask) != 0) {
        	exit(1);
	}

	// parent has the highest prioity when child is being paused
	struct sched_param param;
        param.sched_priority= 98;
        sched_setscheduler(0, SCHED_FIFO, &param);			

	int numOfJob;
	char line[55];
	char *ptr;
	fgets(line, 50, stdin);
	fgets(line, 50, stdin);
	ptr = strtok(line, "\n");
	numOfJob = atoi(ptr);
    
	job *head = (job*)malloc(sizeof(job));
	head->next = NULL;

	char tagList[numOfJob][5];
	int rt, et;

	// construct job queue
	for (int i = 0; i < numOfJob; i++) {
		job *new_job = (job*)malloc(sizeof(job));
        	new_job->next = NULL;
            
       		fgets(line, 50, stdin);
        
	        strcpy(tagList[i], strtok(line, " "));
    
        	ptr = strtok(NULL, " ");
	        rt = atoi(ptr);
    
        	ptr = strtok(NULL, " ");
	        et = atoi(ptr);    

		new_job->pTag = tagList[i];
		new_job->ready_time = rt;
		new_job->exec_time = et;

		insertJob(head, new_job);
   	}	

	process *readyQueueHead = (process*)malloc(sizeof(process));
	readyQueueHead->next = NULL;

	int pid;
	job *current = head->next;
	
	process *readyQueueTail = NULL;
    
	int timer = 0;   
	int clock = 0;
	int executing = 0;
    	int remaining_process = numOfJob;

	current = head->next;
	int child_id;

	// keep waiting for the first job to be ready
	while (current->ready_time >= clock) {
              wait_for_interval();
              clock++;
        } 

	// add the ready job to ready queue
	child_id = fork_child(current);
        process *new_process = (process*)malloc(sizeof(process));
        new_process->pID = child_id;
        new_process->next = NULL;
        readyQueueHead->next = new_process;
        readyQueueTail = new_process;
        current = current->next;

	// reloop after each clocktime (one time_unit)
    	while (remaining_process > 0) {
        	if (executing) {
        		if (current != NULL) {
				if (clock >= current->ready_time) {
					child_id = fork_child(current);
					process *new_process = (process*)malloc(sizeof(process));
        				new_process->pID = child_id;
	        			new_process->next = NULL;

					if (readyQueueHead->next == NULL)
                                		readyQueueTail = readyQueueHead;

        				readyQueueTail->next = new_process;
 	       				readyQueueTail = new_process;
						current = current->next;
				}
			}
        	
			if (waitpid(executing, NULL, WNOHANG) == 0) {
				if (timer >= 500) {
					pause_exec(executing);
					process *reloop_process = (process*)malloc(sizeof(process));
        	        		reloop_process->pID = executing;
		                	reloop_process->next = NULL;

        	        		if (readyQueueHead->next == NULL) 
        		        		readyQueueTail = readyQueueHead;
 		
        		       		readyQueueTail->next = reloop_process;
	                		readyQueueTail = reloop_process;
                			executing = 0;
				}
			}
			
           		else {
				remaining_process--;
	               		timer = 0;
        	        	executing = 0;

				if (remaining_process == 0) {
					// parent as the scheduler finishes its duty here
                        		free(current);
					free(head);
					free(readyQueueHead);
					free(readyQueueTail);
                         		return 0;
                 		}
           		}
	 	}

        	if (!executing) {
            		if (readyQueueHead->next != NULL) { 
		    		pid = (readyQueueHead->next)->pID;
	        	        executing = pid;
        	        	readyQueueHead->next = (readyQueueHead->next)->next;    
            		} 
		}
		
		wake_up(executing);
		wait_for_interval(); 
		timer++;
	        clock++;

    	}
}

