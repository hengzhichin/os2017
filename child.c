#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>

int main(int argc, char** argv)
{

	pid_t pid = getpid(); 
	int NEEDTIME = atoi(argv[2]);
	int x;
	while (1) {
		int ret = scanf("%d", &x);

		for (int j = x; j > 0; j--) {
			volatile unsigned long i; 
			for(i = 0; i < 1000000UL; i++);
			NEEDTIME--;


			if (NEEDTIME <= 0) {
				printf("%s %d\n", argv[1], pid);
				exit(0);
			}

		}


		struct sched_param param;
		param.sched_priority = 97;
		if(sched_setscheduler(0, SCHED_FIFO, & param) < 0){
			exit(EXIT_FAILURE);
		}
	}

}
