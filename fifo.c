
#define _GNU_SOURCE
#include "fifo.h"
#include <sys/syscall.h>
#include <unistd.h>
#include <stdio.h> 
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>

#define MAX_BUF 100

static int cmp(const void* a, const void* b) {
        return((jobs *)a)->ready_time > ((jobs *)b)->ready_time ? 1:-1;
}



int runfork(jobs* process, int ind) {
  int pid;

  if (pid = fork())
    {
    return pid;
    } 
    else
     {
       syscall(341,&process[ind].time[0]);
//       printf("[test] %d %ld.%09ld\n", process[ind].pid,
//	                            process[ind].time[0].tv_sec, process[ind].time[0].tv_nsec);
       process[ind].pid=getpid();
       printf("%s %d\n", process[ind].pname, process[ind].pid);
       cpu_set_t cpuset;
       CPU_ZERO(&cpuset);
       CPU_SET(1, &cpuset);
       if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) != 0) 
          {
	     fprintf(stderr, "sched_setaffinity error\n");
	     exit(1);
          }
    
       while (process[ind].exec_time > 0) 
          {
	     volatile unsigned long i; 
	     for(i = 0; i<1000000UL ; i++);
             --process[ind].exec_time;
          }
       syscall(341,&process[ind].time[1]); 
       char str[100];
       sprintf(str, "[Project1] %d %ld.%09ld %ld.%09ld\n", process[ind].pid,
	                            process[ind].time[0].tv_sec, process[ind].time[0].tv_nsec,process[ind].time[1].tv_sec, process[ind].time[1].tv_nsec);
       syscall(340,str);
       exit(0);
     }
   
  exit(0);
}

int FIFO() 
{
	
    struct sched_param param;
    int i;
    int nn;
    scanf("%d", &nn);
    jobs* process = (jobs*)malloc(nn * sizeof(jobs));

    for (i = 0; i < nn; i++)
        {
		scanf("%s %d %d", process[i].pname, &process[i].ready_time, &process[i].exec_time);
	}

     qsort(process, nn, sizeof(jobs), cmp);
     cpu_set_t cpuset;
     CPU_ZERO(&cpuset);
     CPU_SET(0, &cpuset);
     if (sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpuset) != 0) 
           {
	     fprintf(stderr, "sched_setaffinity error\n");
	     exit(1);
           }
 
     int timer = 0;
     int ind;
     for (ind = 0; ind < nn; ++ind)
        {
           while (process[ind].ready_time > timer)
               {
		volatile unsigned long i; 
		for(i = 0; i<1000000UL ; i++);
		++timer;           
               }

          struct sched_param param;
          param.sched_priority = 99-ind;
          process[ind].pid = runfork(process,ind);
          if (sched_setscheduler( process[ind].pid, SCHED_FIFO, &param) != 0) 
               {
	         fprintf(stderr, "sched_setscheduler error\n");
	         exit(0);
               }          
        }
     for (int ii = 0; ii < nn; ++ii)
        {
        wait(NULL);
        }     

	free(process);
	return 0;
}

