#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include "sjf.h"
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>

#define name_max  32
#define s_s sched_setscheduler
const int BUFF = 1E4;


/*
 * syscall 340 = printk
 *
 *
 */


typedef struct {
    pid_t pid;
    int fd[2];
    int RT;
    char name[name_max];
    int ET;
    struct timespec time[2];
    int flag;
} PS;



static void raised(pid_t pid, int priority) {
    struct sched_param param;
    param.sched_priority = priority;
    s_s(pid, SCHED_FIFO, &param);  // sjf is similar to fifo.


}


static void myscanf(PS *schedule, int JOB_N) {

    for (int i = 0; i < JOB_N; i++) {
        scanf("%s %d %d", schedule[i].name, &schedule[i].RT, &schedule[i].ET);
        pipe(schedule[i].fd);
        schedule[i].flag = 1;
    }

}


static int RT_CMP(const void *a, const void *b) {
    PS *P1 = (PS *) a;
    PS *P2 = (PS *) b;
    if (P1->RT > P2->RT)
        return 1;
    if (P1->RT < P2->RT)
        return -1;

    return 0;
}

static int sjfCMP(const void *a, const void *b) {
    PS *aa = (PS *) a;
    PS *bb = (PS *) b;
    if (aa->ET > bb->ET)
        return 1;
    if (aa->ET < bb->ET)
        return -1;

    return 0;
}


static void toFork(PS *schedule, int *NEXT, int CLOCK, int JOB_N) {

    char buffer[BUFF];
    while (*NEXT < JOB_N && CLOCK == schedule[*NEXT].RT) {
        schedule[*NEXT].pid = fork();
        if (schedule[*NEXT].pid < 0) {
            perror("fork GG\n");
            exit(EXIT_FAILURE);
        }

        if (schedule[*NEXT].pid == 0) {
            cpu_set_t cpuset;
            CPU_ZERO(&cpuset);
            CPU_SET(1, &cpuset);
            close(schedule[*NEXT].fd[1]);   // close child sent port!

            memset(buffer, 0, BUFF);
            sprintf(buffer, "%d", schedule[*NEXT].ET);
            dup2(schedule[*NEXT].fd[0], STDIN_FILENO);      // child stdin open

            if (execl("./child", "child", schedule[*NEXT].name, buffer, (char *) 0) < 0) {   //  stdin Px
                perror("exec gg \n");
                exit(EXIT_FAILURE);
            }
        }

        if (schedule[*NEXT].pid > 0) {
            close(schedule[*NEXT].fd[0]);   //  close pipe receive
            *NEXT = *NEXT + 1;
        }
    }
}

static int TIMECONTROL(PS *schedule, int JOB_N, int NEXT, int NOW, int CLOCK) {
    if (NEXT == JOB_N)
        return schedule[NOW].ET;
    if (schedule[NEXT].RT - CLOCK < schedule[NOW].ET)
        return schedule[NEXT].RT - CLOCK;
    return schedule[NOW].ET;

}

static void Loop(PS *schedule, int NOW, int NEXT, int JOB_N, int *CLOCK) {
    if (NOW == 0 && NEXT < JOB_N)
        while (*CLOCK < schedule[NEXT].RT) {
            volatile unsigned long i;
            for (i = 0; i < 1000000UL; i++);
            *CLOCK = (*CLOCK) + 1;
        }
}

static void update(PS *schedule, int NEXT, int *NOW, int JOB_N, int *CLOCK) {
    int time = TIMECONTROL(schedule, JOB_N, NEXT, *NOW, *CLOCK);
    char buffer[BUFF];
    memset(buffer, 0, BUFF);
    sprintf(buffer, "%d ", time);

    write(schedule[*NOW].fd[1], buffer, strlen(buffer));


    struct sched_param childp;
    childp.sched_priority = 98 + 1;

    if (s_s(schedule[*NOW].pid, SCHED_FIFO, &childp)) {
        perror("setscheduler gg\n");
        exit(EXIT_FAILURE);
    }

    *CLOCK = (*CLOCK) + time;
    schedule[*NOW].ET = schedule[*NOW].ET - time;

    if (schedule[*NOW].ET == 0) {

        wait(NULL);
        clock_gettime(0, &schedule[*NOW].time[1]);
        *NOW = (*NOW) + 1;
    }
}


static void result(int JOB_N, PS *schedule) {


    for (int i = 0; i < JOB_N; i++) {
        char str[BUFF];
        sprintf(str, "[Project1] %d %ld.%09ld %ld.%09ld \n", schedule[i].pid, schedule[i].time[0].tv_sec,
                schedule[i].time[0].tv_nsec, schedule[i].time[1].tv_sec,
                schedule[i].time[1].tv_nsec);
        syscall(340, str);
    }


}

static void resort(PS *schedule, int NOW, int NEXT, int flag,int p_or_s) {
    if (flag == 1) {
        qsort(&schedule[NOW], NEXT - NOW, sizeof(PS), sjfCMP);
        clock_gettime(0, &schedule[NOW].time[0]);
        if(p_or_s==0)
            schedule[NOW].flag = 0;
        return;
    }
    qsort(&schedule[NOW + 1], NEXT - NOW - 1, sizeof(PS), sjfCMP);

}

int PORSJF(int p_or_s) {

    // p_or_s = 1 means enable preemptive

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);


    raised(0, 98);
    int JOB_N = 0;
    scanf("%d", &JOB_N);
    PS *schedule = (PS *) malloc(JOB_N * sizeof(PS));
    myscanf(schedule, JOB_N);
    qsort(schedule, JOB_N, sizeof(PS), RT_CMP);


    int NEXT = 0;
    int NOW = 0;
    int CLOCK = 0;


    while (NOW < JOB_N) {
        Loop(schedule, NOW, NEXT, JOB_N, &CLOCK);
        toFork(schedule, &NEXT, CLOCK, JOB_N);
        resort(schedule, NOW, NEXT, schedule[NOW].flag, p_or_s);
        update(schedule, NEXT, &NOW, JOB_N, &CLOCK);
    }

// print
    result(JOB_N, schedule);
    free(schedule);

    return 0;
}
